FROM python:slim

# install basic utilities
RUN \
  apt-get -qq -y update && \
  apt-get -qq -y upgrade && \
  apt-get install -y \
    jq git tree hdf5-tools bash-completion \
    pkg-config cmake g++ libkrb5-dev uuid-dev \
    libxml2-dev libssl-dev libsystemd-dev zlib1g-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt-get/lists/* && \
  true


# python installs
COPY requirements.txt .
RUN \
  pip3 install --upgrade pip && \
  pip3 install -r requirements.txt

# user setup
RUN \
  echo '%sudo	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers && \
  useradd -m atlas && \
  usermod -aG sudo atlas && \
  true
ENV USER=atlas HOME=/home/atlas
COPY custom-rc.bash ${HOME}/.bashrc

# less stable program setup
ADD https://raw.githubusercontent.com/dguest/_h5ls/master/_h5ls.sh /etc/
RUN chmod a+r /etc/_h5ls.sh

# env setup
USER atlas
WORKDIR ${HOME}

CMD /bin/bash --rcfile /home/atlas/.bashrc
